#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 14:12:58 2021

@author: flora



recherche dans le lexique 380
recherche par forme orthographique ou phonème avec des options
- discontinuité (pas à la suite)
- permutation (pas dans l'ordre)
- neutralisation

appariemment de quelques phonèmes

écran de démarrage Buis les Baronnies le 23 octobre 2021  © Julien Tranchard
Rabonnir = rendre meilleur
se rabonnir = devenir meilleur

Attention sous windows, les ouverture de fichier doivent être accompagnées de encoding="UTF-8"

"""

import csv
from tkinter import *
from PIL import ImageTk, Image
import os, os.path
from tkinter import messagebox
import webbrowser
import codecs
from datetime import datetime
import subprocess

# pour permuter les listes
import itertools


def manuel(fen): 
    c.delete(ALL)
    c.create_image(0, 0, image = photo, anchor=NW)
    webbrowser.open('tutoriel_rabonni.pdf')
           
def apropos(fen): 
    messagebox.showinfo("A propos", "Version du 02 janvier 2023, à Angers\nil fait 12°C\nla pression est de 1023 hPa\nle soleil s'est levé à 8h27 et se couchera à 17h28\nfond d'écran Buis les Baronnies le 23 octobre 2021 (© Julien Tranchard) \nRabonnir : rendre meilleur")
    
def append(letter, field):    
    field.insert(INSERT, letter)
    
def ortho2phon(ortho, field):
    infile = open("Lexique380_sansDoublon_V2_utf8_colPhon_encours.csv", "r", encoding="UTF-8")
    csvfile = csv.reader(infile, delimiter=';')
    
    
    field.delete("0", INSERT)
    phon = "erreur"
    for line in csvfile:
        forme = line[0].strip()
        if forme == ortho:
            phon = line[38].strip()
    field.insert(INSERT, phon)

"""
Scrip : ouvrir le dossier où se trouve les resultats
"""
def openRslt(fen, fname):
    outRep = "output/"
    subprocess.Popen('explorer "'+outRep+'"')

def lancement(nbCL, nbCS, nbCM, nbVA, nbVL, nbVN, neut, dis, perm, value, mot):
    c.delete(ALL)
    c.create_image(0, 0, image = photo, anchor=NW)
    
    
    poly = ""
    if dis == 1:
        poly+="D"
    if perm == 1:
        poly+="P"
    poly+=neut
    
    infile = open("Lexique380_sansDoublon_V2_utf8_colPhon_encours.csv", "r", encoding="UTF-8")
    outfile = open("output/Rabonni_"+now+"_"+value+"_"+poly+".csv","w", encoding="UTF-8")
    
    filecsv = csv.reader(infile, delimiter=';')
    
    header = next(filecsv, None)
    
    newheader = "Unité testée (orthographique);Unité testée (phonologique);Unité de sortie (orthographique);Unité de sortie (phonologique);Fréquence;Nombre de phonèmes;Type de polymorphie;Part partagée;Alternance phonémique;C Sonorisation neutralisée;C Mode neutralisé;C Lieu neutralisé;V Nasalité neutralisée;V Aperture neutralisée;V Lieu neutralisé;Total;Part neutralisée;Traces sémantiques" 
    outfile.write(newheader+"\n")
    
    compteur = 0
    listeSortie = []
    for line in filecsv:
        if dis == 1 and perm == 0:
            i=0
            listtest = []
            variable = line[37]
            rslt = True
            while i < len(value):
                test = variable.find(value[i])
                if test != -1:
                    variable = variable[test+1:]
                else :
                    rslt = False
                    
                i+=1
                
            if rslt != False :
                
                if line[0] not in listeSortie:
                    compteur +=1
                    ecritcsv(outfile, line, mot, value, poly, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN)
                    listeSortie.append(line[0])
            
        if perm == 1 and dis == 0:
            #toutes les permutations possibles
            permutations = list(itertools.permutations(value))
            res = [''.join(tups) for tups in permutations]
            #on supprime les doublons
            res2 = []
            for tup in res:
                if tup not in res2:
                    res2.append(tup)
            for permutation in res2:
                if permutation in line[37]:
                    
                    if line[0] not in listeSortie:
                        compteur +=1
                        ecritcsv(outfile, line, mot, value, poly, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN)      
                        listeSortie.append(line[0])
                    
        if perm ==1 and dis ==1 :
            i=0
            listtest = []
            variable = line[37]
            while i < len(value):
                test = variable.find(value[i])
                listtest.append(test)
                variable = variable.replace(value[i], '', test)
                i+=1
                
            if -1 not in listtest:
                
                if line[0] not in listeSortie:
                    compteur +=1
                    ecritcsv(outfile, line, mot, value, poly, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN)                
                    listeSortie.append(line[0])
        
        if dis == 0 and perm == 0 :
            if value in line[37]:
                
                if line[0] not in listeSortie:
                    compteur +=1
                    ecritcsv(outfile, line, mot, value, poly, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN)
                    listeSortie.append(line[0])
    c.create_text(10, 10, anchor=NW, text="Résultats disponibles, votre fichier contient "+str(compteur)+" unités", font=("calibri", 12))
 
def ecritcsv(outfile, line, mot, value, poly, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN):
    nball=0
    nball = nbCS+nbCM+nbCL+nbVN+nbVA+nbVL
    
    outfile.write(mot+";")
    outfile.write(value+";")
    outfile.write(line[0]+";")
    outfile.write(line[37]+";")
    outfile.write(line[6]+";")
    outfile.write(line[15]+";")
                    
    outfile.write(poly+";")
    
    part = len(value) / int(line[15])
    outfile.write(str(part)+";")
    
    outfile.write("0;")
    outfile.write(str(nbCS)+";"+str(nbCM)+";"+str(nbCL)+";"+str(nbVN)+";"+str(nbVA)+";"+str(nbVL)+";"+str(nball))
    partneut = nball/len(line[37])
    outfile.write(";"+str(partneut))
    outfile.write("\n")
    
    

def neutralisation(fen, fname):
    c.delete(ALL)
    c.create_image(0, 0, image = photo, anchor=NW)
    
    c.create_text(10, 10, anchor=NW, text="Etape de création d'un tableur avec neutralisation : ", font=("calibri", 10))
    
    checkButtonsVars = {}
    # "²", "h", "x", "&" 
    listeSigne = ["é", "~", '"', "#", "'", "{", "}", "(", ")", "[", "]", "-", "|", "è", "`", "_", "\\", "ç", "^", "à", "@", "=", "+", "$", "£", "¤", "%", "ù", "*", "?", ",", ".", ";", "/", ":", "!", "§", "<", ">", "2", "3", "4", "5", "6", "7", "8", "9"]
    nbsigne = 0
    
    c.create_text(10, 50, anchor=NW, text="* Consonnes / sonorisation : ", font=("calibri", 10))
    nbC = 80
    for itemCS in ['pb', 'td', 'kg', 'fv', 'sz', 'ʃʒ']:
        c.create_text(50, nbC, anchor=NW, text=itemCS, font=("calibri", 11))
        c1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=c1)
        c.create_window(100, nbC, anchor=NW, window=checkbutton)
        c2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=c2)
        c.create_window(150, nbC, anchor=NW, window=checkbutton)
        nbC+=30
        
        checkButtonsVars[itemCS+listeSigne[nbsigne]] = [c1, c2]
        nbsigne+=1
        
    c.create_text(300, 50, anchor=NW, text="* Consonnes / mode : ", font=("calibri", 10))
    nbC = 80
    for itemCM in ['pt', 'pk', 'kt', 'bd', 'bg', 'dg', 'fs', 'fʃ', 'sʃ', 'vz', 'vʒ', 'zʒ', 'nm', 'nɲ', 'nŋ', 'mɲ', 'mŋ', 'ɲŋ']:
        c.create_text(350, nbC, anchor=NW, text=itemCM, font=("calibri", 11))
        c1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=c1)
        c.create_window(400, nbC, anchor=NW, window=checkbutton)
        c2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=c2)
        c.create_window(450, nbC, anchor=NW, window=checkbutton)
        nbC+=30
        
        checkButtonsVars[itemCM+listeSigne[nbsigne]] = [c1, c2]
        nbsigne+=1
        
    c.create_text(600, 50, anchor=NW, text="* Consonnes / lieu : ", font=("calibri", 10))
    nbC = 80
    for itemCL in ['ts', 'dz', 'dl', 'lz', 'kʃ', 'gʒ']:
        c.create_text(650, nbC, anchor=NW, text=itemCL, font=("calibri", 11))
        c1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=c1)
        c.create_window(700, nbC, anchor=NW, window=checkbutton)
        c2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=c2)
        c.create_window(750, nbC, anchor=NW, window=checkbutton)
        nbC+=30
        
        checkButtonsVars[itemCL+listeSigne[nbsigne]] = [c1, c2]
        nbsigne+=1

    c.create_text(900, 50, anchor=NW, text="* Voyelles / aperture : ", font=("calibri", 10))
    nbV = 80
    for itemVA in ['iy', 'iu', 'uy', 'oɛ', 'ɛə', 'əo']:
        c.create_text(950, nbV, anchor=NW, text=itemVA, font=("calibri", 11))
        v1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=v1)
        c.create_window(1000, nbV, anchor=NW, window=checkbutton)
        v2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=v2)
        c.create_window(1050, nbV, anchor=NW, window=checkbutton)
        nbV+=30
        
        checkButtonsVars[itemVA+listeSigne[nbsigne]] = [v1, v2]
        nbsigne+=1
    
    c.create_text(1200, 50, anchor=NW, text="* Voyelles / lieu : ", font=("calibri", 10))
    nbV = 80
    for itemVL in ['iɛ', 'ia', 'ɛa', 'yə', 'uo', 'ua', 'oa']:
        c.create_text(1250, nbV, anchor=NW, text=itemVL, font=("calibri", 11))
        v1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=v1)
        c.create_window(1300, nbV, anchor=NW, window=checkbutton)
        v2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=v2)
        c.create_window(1350, nbV, anchor=NW, window=checkbutton)
        nbV+=30
        
        checkButtonsVars[itemVL+listeSigne[nbsigne]] = [v1, v2]
        nbsigne+=1
    
    c.create_text(1500, 50, anchor=NW, text="* Voyelles / nasalisation : ", font=("calibri", 10))
    nbV = 80
    for itemVN in ['Aa', 'Oo', 'Eɛ', 'Eə']:
        c.create_text(1550, nbV, anchor=NW, text=itemVN, font=("calibri", 11))
        v1 = IntVar()
        checkbutton = Checkbutton(text=" > ", variable=v1)
        c.create_window(1600, nbV, anchor=NW, window=checkbutton)
        v2 = IntVar()
        checkbutton = Checkbutton(text=" < ", variable=v2)
        c.create_window(1650, nbV, anchor=NW, window=checkbutton)
        nbV+=30
        
        checkButtonsVars[itemVN+listeSigne[nbsigne]] = [v1, v2]
        nbsigne+=1
    
    #print(checkButtonsVars)
    go = Button(c, text="Vers étape 2", command=lambda: de1a2(fen, fname, checkButtonsVars))
    c.create_window(500, 10, anchor=NW, window=go)


def de1a2(fen, fname, checkButtonsVars):
    c.delete(ALL)
    c.create_image(0, 0, image = photo, anchor=NW)
    
    infile = open("Lexique380_sansDoublon_V2_utf8_colPhon.csv","r")
    outfile = open("Lexique380_sansDoublon_V2_utf8_colPhon_encours.csv","w")
    
    filecsv = csv.reader(infile, delimiter=';')
    outcsv = csv.writer(outfile, delimiter=";")
    	
    liste = []
    dictSigne = {}
    
    listeCS = ['pb', 'td', 'kg', 'fv', 'sz', 'ʃʒ']
    listeCM = ['pt', 'pk', 'kt', 'bd', 'bg', 'dg', 'fs', 'fʃ', 'sʃ', 'vz', 'vʒ', 'zʒ', 'nm', 'nɲ', 'nŋ', 'mɲ', 'mŋ', 'ɲŋ']
    listeCL = ['ts', 'dz', 'dl', 'lz', 'kʃ', 'gʒ']
    listeVA = ['iy', 'iu', 'uy', 'oɛ', 'ɛə', 'əo']
    listeVL = ['iɛ', 'ia', 'ɛa', 'yə', 'uo', 'ua', 'oa']
    listeVN = ['Aa', 'Oo', 'Eɛ', 'Eə']
    
    nbCS = 0
    nbCM = 0
    nbCL = 0
    nbVA = 0
    nbVL = 0
    nbVN = 0
	
    header = next(filecsv, None)
    if header:
        header.append("")
        header[38]="39_graphieEncours"
                      
    liste.append(header)
    
    for var, check in checkButtonsVars.items():
        if var[0]+var[1] in listeCS and check[0].get() ==1:
            nbCS+=1
        if var[0]+var[1] in listeCS and check[1].get() ==1:
            nbCS+=1
        if var[0]+var[1] in listeCL and check[0].get() ==1:
            nbCL+=1
        if var[0]+var[1] in listeCL and check[1].get() ==1:
            nbCL+=1
        if var[0]+var[1] in listeCM and check[0].get() ==1:
            nbCM+=1 
        if var[0]+var[1] in listeCM and check[1].get() ==1:
            nbCM+=1
        if var[0]+var[1] in listeVA and check[0].get() ==1:
            nbVA+=1
        if var[0]+var[1] in listeVA and check[1].get() ==1:
            nbVA+=1
        if var[0]+var[1] in listeVL and check[0].get() ==1:
            nbVL+=1
        if var[0]+var[1] in listeVL and check[1].get() ==1:
            nbVL+=1
        if var[0]+var[1] in listeVN and check[0].get() ==1:
            nbVN+=1
        if var[0]+var[1] in listeVN and check[1].get() ==1:
            nbVN+=1 
	
    for line in filecsv:
        line.append("")
        
        usuel = line[37]  
        for var, check in checkButtonsVars.items():
            
            if check[0].get() == 1:
                usuel = usuel.replace(var[0], var[2]+"0")
                dictSigne[var[2]+"0"] = var[1]
            
            if check[1].get() == 1:
                usuel = usuel.replace(var[1], var[2]+"1")
                dictSigne[var[2]+"1"] = var[0]
        
        line[38] = usuel
        
        for signe, lettre in dictSigne.items():
            line[38] = line[38].replace(signe,lettre)
        liste.append(line)
    nball = 0
    nball = nbCL+nbCS+nbCM+nbVA+nbVL+nbVN
    neut = ""
    print(nball)
    #print(dictSigne)
    if nball > 0 :
        neut = "N"
    outcsv.writerows(liste)
    copresence(fen, fname, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN, neut)
    	    
    
def copresence(fen, fname, nbCL, nbCS, nbCM, nbVA, nbVL, nbVN, neut):
    c.delete(ALL)
    c.create_image(0, 0, image = photo, anchor=NW)
    
    nbVY = 30
    nbC = 30
    checkButtonsVars = {}
    
    
    c.create_text(10, 10, anchor=NW, text="forme orthographique testée : ", font=("calibri", 12))
    valueFOrtho = StringVar() 
    e0 = Entry(textvariable=valueFOrtho)
    c.create_window(310, 10, anchor=NW, window=e0)
    conv = Button(c, text="convertir en phonèmes", command=lambda: ortho2phon(valueFOrtho.get(),e1))
    c.create_window(480, 7, anchor=NW, window=conv)
    
    c.create_text(10, 50, anchor=NW, text="Phonèmes : ", font=("calibri", 12))
    
    a = Button(c, text="a", command=lambda: append("a", e1))
    E = Button(c, text="ɛ/e", command=lambda: append("ɛ", e1))
    on = Button(c, text="ɔ̃", command=lambda: append("O", e1))
    j = Button(c, text="j", command=lambda: append("j", e1))
    O = Button(c, text="o/ɔ", command=lambda: append("o", e1))
    i = Button(c, text="i", command=lambda: append("i", e1))
    ein = Button(c, text="ɛ̃/œ̃", command=lambda: append("E", e1))
    u = Button(c, text="u", command=lambda: append("u", e1))
    an = Button(c, text="ɑ̃", command=lambda: append("A", e1))
    ee = Button(c, text="ə/œ/ø", command=lambda: append("ə", e1))
    w = Button(c, text="w", command=lambda: append("w", e1))
    y = Button(c, text="y", command=lambda: append("y", e1))
    ue = Button(c, text="ɥ", command=lambda: append("ɥ", e1))
    k = Button(c, text="k", command=lambda: append("k", e1))
    p = Button(c, text="p", command=lambda: append("p", e1))
    l = Button(c, text="l", command=lambda: append("l", e1))
    t = Button(c, text="t", command=lambda: append("t", e1))
    R = Button(c, text="ʁ", command=lambda: append("ʁ", e1))
    f = Button(c, text="f", command=lambda: append("f", e1))
    s = Button(c, text="s", command=lambda: append("s", e1))
    d = Button(c, text="d", command=lambda: append("d", e1))
    Z = Button(c, text="ʒ", command=lambda: append("ʒ", e1))
    n = Button(c, text="n", command=lambda: append("n", e1))
    b = Button(c, text="b", command=lambda: append("b", e1))
    v = Button(c, text="v", command=lambda: append("v", e1))
    g = Button(c, text="g", command=lambda: append("g", e1))
    m = Button(c, text="m", command=lambda: append("m", e1))
    z = Button(c, text="z", command=lambda: append("z", e1))
    S = Button(c, text="ʃ", command=lambda: append("ʃ", e1))
    N = Button(c, text="ɲ", command=lambda: append("ɲ", e1))
    G = Button(c, text="ŋ", command=lambda: append("ŋ", e1))
    c.create_window(10, 70, anchor=NW, window=p)
    c.create_window(10, 100, anchor=NW, window=b)
    c.create_window(10, 130, anchor=NW, window=t)
    c.create_window(10, 160, anchor=NW, window=d)
    c.create_window(10, 190, anchor=NW, window=k)
    c.create_window(10, 220, anchor=NW, window=g)
    c.create_window(50, 70, anchor=NW, window=f)
    c.create_window(50, 100, anchor=NW, window=v)
    c.create_window(50, 130, anchor=NW, window=s)
    c.create_window(50, 160, anchor=NW, window=z)
    c.create_window(50, 190, anchor=NW, window=S)
    c.create_window(50, 220, anchor=NW, window=Z)
    c.create_window(90, 70, anchor=NW, window=l)
    c.create_window(90, 100, anchor=NW, window=R)
    c.create_window(90, 130, anchor=NW, window=ue)
    c.create_window(90, 160, anchor=NW, window=w)
    c.create_window(90, 190, anchor=NW, window=j)
    c.create_window(130, 70, anchor=NW, window=n)
    c.create_window(130, 100, anchor=NW, window=m)
    c.create_window(130, 130, anchor=NW, window=N)
    c.create_window(130, 160, anchor=NW, window=G)
    c.create_window(175, 70, anchor=NW, window=i)
    c.create_window(175, 100, anchor=NW, window=y)
    c.create_window(175, 130, anchor=NW, window=u)
    c.create_window(175, 160, anchor=NW, window=E)
    c.create_window(175, 190, anchor=NW, window=ee)
    c.create_window(175, 220, anchor=NW, window=O)
    c.create_window(175, 250, anchor=NW, window=a)
    c.create_window(215, 70, anchor=NW, window=on)
    c.create_window(215, 100, anchor=NW, window=ein)
    c.create_window(215, 130, anchor=NW, window=an)
    
    c.create_text(10, 290, anchor=NW, text="Séquence à rechercher : ", font=("calibri", 12))
    c.create_text(400, 290, anchor=NW, text="dans l'ordre de la suite", font=("calibri", 12))
    
    valueRch = StringVar() 
    e1 = Entry(textvariable=valueRch)
    c.create_window(250, 290, anchor=NW, window=e1)
    
    c.create_text(10, 330, anchor=NW, text="Options :", font=("calibri", 12))
    opt1dis = IntVar()
    opt2perm = IntVar()
    #opt3neut = IntVar()
    checkbutton = Checkbutton(text=" discontinuité (pas à la suite)", variable=opt1dis)
    c.create_window(10, 360, anchor=NW, window=checkbutton)
    
    checkbutton2 = Checkbutton(text=" permutation (pas dans l'ordre)", variable=opt2perm)
    c.create_window(10, 390, anchor=NW, window=checkbutton2) 
    
    
    go = Button(c, text="C'est parti !", command=lambda: lancement(nbCL, nbCS, nbCM, nbVA, nbVL, nbVN, neut, opt1dis.get(), opt2perm.get(), valueRch.get(), valueFOrtho.get()))
    c.create_window(600, 130, anchor=NW, window=go)
    
def graphie(fen, fname):
    infile = open("Lexique380_sansDoublon_V2_utf8.csv", "r")
    outfile = open("Lexique380_sansDoublon_V2_utf8_colPhon.csv","w")
    
    filecsv = csv.reader(infile, delimiter=';')
    outcsv = csv.writer(outfile, delimiter=";")
    	
    liste = []
		
    header = next(filecsv, None)
    if header:
        header.append("")
        header[37]="38_graphieUsuelle"
                      
    liste.append(header)
			
    for line in filecsv:
        line.append("")
        
        line[37]=line[1].replace("S", "ʃ").replace("Z", "ʒ").replace("§", "ɔ̃").replace("R", "ʁ").replace("8", "ɥ").replace("N", "ɲ").replace("G", "ŋ").replace("E", "ɛ").replace("e", "ɛ").replace("5", "ɛ̃").replace("1", "ɛ̃").replace("@", "ɑ̃").replace("°", "ə").replace("9", "ə").replace("2", "ə").replace("O", "o")
        line[37]=line[37].replace("ɔ̃", "O").replace("ɛ̃", "E").replace("ɑ̃", "A")
        
        liste.append(line)
				
    outcsv.writerows(liste)
    
    
    
if __name__ == "__main__":

    fen = Tk()
    # les variables suivantes seront utilisées de manière globale :
    fen.rep=os.getcwd()
    fen.fic=""
    field = Entry(fen)
    fname = ""
    
    now = str(datetime.now()).replace(" ", "_").replace(":", "-").replace(".", "")
        
    # creation de la barre de menu:
    menubar = Menu(fen)

    # creation du menu "outils"
    fonctmenu = Menu(menubar, tearoff=0)
    menubar.add_cascade(label="Outils", underline=0, menu=fonctmenu)
    #fonctmenu.add_command(label="Bonne graphie", command=lambda: graphie(fen, fname))
    #fonctmenu.add_command(label="Co-présence de phonèmes", command=lambda: copresence(fen, fname))
    fonctmenu.add_command(label="Co-présence de phonèmes", command=lambda: neutralisation(fen, fname))
    
    # creation du menu "Aide"
    helpmenu = Menu(menubar, tearoff=0)
    menubar.add_cascade(label="Aide", underline=0, menu=helpmenu)
    helpmenu.add_command(label="A propos", command=lambda: apropos(fen))
    helpmenu.add_command(label="Manuel", command=lambda: manuel(fen))
     
    # display the menu
    fen.config(menu=menubar)
       
    
    # ici on crée une boite pouvant etre supprimée à tout moment pour chaque lancement de programme
    c=Canvas(fen,scrollregion=(0,0,1800,1200))
    
    hbar=Scrollbar(fen,orient=HORIZONTAL)
    hbar.pack(side=BOTTOM,fill=X)
    hbar.config(command=c.xview)
    vbar=Scrollbar(fen,orient=VERTICAL)
    vbar.pack(side=RIGHT,fill=Y)
    vbar.config(command=c.yview)
    c.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)
    
    photo=ImageTk.PhotoImage(file="V__65D1.jpg")
    c.create_image(0, 0, image = photo, anchor=NW)
    
    c.config(width=900,height=700)
    c.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)
    c.pack(side=LEFT,expand=True,fill=BOTH)
    
    fen.title("projet Rabonni")    
    fen.iconphoto(False, ImageTk.PhotoImage(file='logo-rabonni.png'))
    fen.mainloop()
    os.system('rm -f Lexique380_sansDoublon_V2_utf8_colPhon_encours.csv')